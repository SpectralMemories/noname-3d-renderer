
#include <stdlib.h>
#include <stdbool.h>
#include <SFML/Graphics.h>

#include "renderer.h"
#include "definitions.h"

static sfRenderWindow* gameWindow;
static sfImage* gameScreenImage;

static mesh* cube;

static void initializeCube ()
{
    cube = malloc (sizeof(mesh));
    cube->anchor = (vector3) {0.0f, 0.0f, 0.0f};
    cube->trisc = 12;
    cube->tris = calloc (12, sizeof(triangle));

    triangle* cubes_triangle = cube->tris;
    //SOUTH
    (*(cubes_triangle + 0)).vertex[0] = (vector3) {0.0f, 0.0f, 0.0f};
    (*(cubes_triangle + 0)).vertex[1] = (vector3) {0.0f, 1.0f, 0.0f};
    (*(cubes_triangle + 0)).vertex[2] = (vector3) {1.0f, 1.0f, 0.0f};

    (*(cubes_triangle + 1)).vertex[0] = (vector3) {0.0f, 0.0f, 0.0f};
    (*(cubes_triangle + 1)).vertex[1] = (vector3) {1.0f, 1.0f, 0.0f};
    (*(cubes_triangle + 1)).vertex[2] = (vector3) {1.0f, 0.0f, 0.0f};
    //
    //EAST
    (*(cubes_triangle + 2)).vertex[0] = (vector3) {1.0f, 0.0f, 0.0f};
    (*(cubes_triangle + 2)).vertex[1] = (vector3) {1.0f, 1.0f, 0.0f};
    (*(cubes_triangle + 2)).vertex[2] = (vector3) {1.0f, 1.0f, 1.0f};

    (*(cubes_triangle + 3)).vertex[0] = (vector3) {1.0f, 0.0f, 0.0f};
    (*(cubes_triangle + 3)).vertex[1] = (vector3) {1.0f, 1.0f, 1.0f};
    (*(cubes_triangle + 3)).vertex[2] = (vector3) {1.0f, 0.0f, 1.0f};
    //
    //NORTH
    (*(cubes_triangle + 4)).vertex[0] = (vector3) {1.0f, 0.0f, 1.0f};
    (*(cubes_triangle + 4)).vertex[1] = (vector3) {1.0f, 1.0f, 1.0f};
    (*(cubes_triangle + 4)).vertex[2] = (vector3) {0.0f, 1.0f, 1.0f};

    (*(cubes_triangle + 5)).vertex[0] = (vector3) {1.0f, 0.0f, 1.0f};
    (*(cubes_triangle + 5)).vertex[1] = (vector3) {0.0f, 1.0f, 1.0f};
    (*(cubes_triangle + 5)).vertex[2] = (vector3) {0.0f, 0.0f, 1.0f};
    //
    //WEST
    (*(cubes_triangle + 6)).vertex[0] = (vector3) {0.0f, 0.0f, 1.0f};
    (*(cubes_triangle + 6)).vertex[1] = (vector3) {0.0f, 1.0f, 1.0f};
    (*(cubes_triangle + 6)).vertex[2] = (vector3) {0.0f, 1.0f, 0.0f};

    (*(cubes_triangle + 7)).vertex[0] = (vector3) {0.0f, 0.0f, 1.0f};
    (*(cubes_triangle + 7)).vertex[1] = (vector3) {0.0f, 1.0f, 0.0f};
    (*(cubes_triangle + 7)).vertex[2] = (vector3) {0.0f, 0.0f, 0.0f};
    //
    //TOP
    (*(cubes_triangle + 8)).vertex[0] = (vector3) {0.0f, 1.0f, 0.0f};
    (*(cubes_triangle + 8)).vertex[1] = (vector3) {0.0f, 1.0f, 1.0f};
    (*(cubes_triangle + 8)).vertex[2] = (vector3) {1.0f, 1.0f, 1.0f};

    (*(cubes_triangle + 9)).vertex[0] = (vector3) {0.0f, 1.0f, 0.0f};
    (*(cubes_triangle + 9)).vertex[1] = (vector3) {1.0f, 1.0f, 1.0f};
    (*(cubes_triangle + 9)).vertex[2] = (vector3) {1.0f, 1.0f, 0.0f};
    //
    //BOTTOM
    (*(cubes_triangle + 10)).vertex[0] = (vector3) {1.0f, 0.0f, 1.0f};
    (*(cubes_triangle + 10)).vertex[1] = (vector3) {0.0f, 0.0f, 1.0f};
    (*(cubes_triangle + 10)).vertex[2] = (vector3) {0.0f, 0.0f, 0.0f};

    (*(cubes_triangle + 11)).vertex[0] = (vector3) {1.0f, 0.0f, 1.0f};
    (*(cubes_triangle + 11)).vertex[1] = (vector3) {0.0f, 0.0f, 0.0f};
    (*(cubes_triangle + 11)).vertex[2] = (vector3) {1.0f, 0.0f, 0.0f};
    //
}

void initializeGame (sfRenderWindow* window, sfImage* image)
{
    gameWindow = window;
    gameScreenImage = image;
    initializeCube();
    initializeRenderer(image, NEAR, FAR, FOV);
}


void updateGame ()
{
    renderMesh(cube);

    if (sfKeyboard_isKeyPressed(sfKeyLeft))
    {
        thetra += 0.01f;
    }
    else if (sfKeyboard_isKeyPressed(sfKeyRight))
    {
        thetra -= 0.01f;
    }
    else
    {
        thetra += 0.005f;
    }
}
