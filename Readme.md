# NoName 3D renderer
## Written in C

As the name implies, this is a no name, no actual use 3D renderer. Its just a demo project

# Requirement:

SFML (for drawing pixels on a 2D plane)
CSFML (C++ -> C binding)

# Compilation:

Requires a C11 compliant C compiler, and the appropriate SFML and CSFML libraries.
If compiling for Windows, the usleep method call (in main.c) might cause problem.