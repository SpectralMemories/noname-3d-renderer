#ifndef RENDERER
    #define RENDERER

    #include "definitions.h"
    #include <SFML/Graphics.h>

    extern float thetra;

    void initializeRenderer (sfImage* image, float near, float far, float _fov);
    void renderMesh (mesh* mesh);
#endif // RENDERER
