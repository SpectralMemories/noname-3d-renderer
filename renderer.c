
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <SFML/Graphics.h>

#include "definitions.h"

static sfImage* renderImage;

static float nearPlane = NEAR;
static float farPlane = FAR;
static float fov = FOV;

static matrix4x4 projectionMatrix;

float thetra;

static triangle* triProjected;
static triangle* triTranslated;
static triangle* triRotatedZ;
static triangle* triRotatedZX;

static matrix4x4* zRotationMatrix;
static matrix4x4* xRotationMatrix;

static void initializeProjectionMatrix ()
{
    float aspectRatio = (float) HEIGHT / (float) WIDTH;
    float fovRad = 1.0f / tanf(fov * 0.5f / 180.0f * PI);

    projectionMatrix.m[0][0] = aspectRatio * fovRad;
    projectionMatrix.m[1][1] = fovRad;
    projectionMatrix.m[2][2] = farPlane / (farPlane - nearPlane);
    projectionMatrix.m[3][2] = (-farPlane * nearPlane) / (farPlane - nearPlane);
    projectionMatrix.m[2][3] = 1.0f;
    projectionMatrix.m[3][3] = 0.0f;
}

static vector3 multiplyMatrixVector (vector3 input, matrix4x4 matrix)
{
    vector3 output;
    output.x = input.x * matrix.m[0][0] + input.y * matrix.m[1][0] + input.z * matrix.m[2][0] + matrix.m[3][0];
    output.y = input.x * matrix.m[0][1] + input.y * matrix.m[1][1] + input.z * matrix.m[2][1] + matrix.m[3][1];
    output.z = input.x * matrix.m[0][2] + input.y * matrix.m[1][2] + input.z * matrix.m[2][2] + matrix.m[3][2];
    float w = input.x * matrix.m[0][3] + input.y * matrix.m[1][3] + input.z * matrix.m[2][3] + matrix.m[3][3];

    if (w != 0.0f)
    {
        output.x /= w;
        output.y /= w;
        output.z /= w;
    }
    return output;
}

static void processRotationMatrix (const float angle, matrix4x4* matRotZ, matrix4x4* matRotX)
{
    // Rotation Z
    matRotZ->m[0][0] = cosf(angle);
    matRotZ->m[0][1] = sinf(angle);
    matRotZ->m[1][0] = -sinf(angle);
    matRotZ->m[1][1] = cosf(angle);
    matRotZ->m[2][2] = 1.0f;
    matRotZ->m[3][3] = 1.0f;

    // Rotation X
    matRotX->m[0][0] = 1.0f;
    matRotX->m[1][1] = cosf(angle * 0.5f);
    matRotX->m[1][2] = sinf(angle * 0.5f);
    matRotX->m[2][1] = -sinf(angle * 0.5f);
    matRotX->m[2][2] = cosf(angle * 0.5f);
    matRotX->m[3][3] = 1.0f;
}

static void drawPixel (int x, int y)
{
    if(x > WIDTH || x < 0 || y > HEIGHT || y < 0)
    {
        return; //out of bound, don't bother
    }

    sfImage_setPixel(renderImage, x, y, FOREGROUND);
}

//Basic Bresenham's line algorithm
static void drawLine (vector3 a, vector3 b) //This method is causing trouble
{
    int x0 = (int) round(a.x);
    int y0 = (int) round(a.y);

    int x1 = (int) round(b.x);
    int y1 = (int) round(b.y);

    int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
    int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1;
    int err = (dx>dy ? dx : -dy)/2, e2;

    for (int l = 0; l < WIDTH; l++) //Sometimes gets stuck
        //for now F it, i'll just limit the maximum iteration count
    {
        drawPixel(x0,y0);
        if (x0==x1 && y0==y1)
        {
            break;
        }
        e2 = err;
        if (e2 >-dx)
        {
            err -= dy; x0 += sx;
        }
        if (e2 < dy)
        {
            err += dx; y0 += sy;
        }
    }
}

static void drawTriangle (triangle* triangle)
{
    drawLine(triangle->vertex[0], triangle->vertex[1]);
    drawLine(triangle->vertex[1], triangle->vertex[2]);
    drawLine(triangle->vertex[2], triangle->vertex[0]);
}

void initializeRenderer (sfImage* image, float near, float far, float _fov)
{
    renderImage = image;

    nearPlane = near;
    farPlane = far;
    fov = _fov;

    initializeProjectionMatrix();

    zRotationMatrix = calloc (1, sizeof(matrix4x4));
    xRotationMatrix = calloc (1, sizeof(matrix4x4));

    triProjected = calloc (1, sizeof(triangle));
    triRotatedZ = calloc (1, sizeof(triangle));
    triRotatedZX = calloc (1, sizeof(triangle));
    triTranslated = calloc (1, sizeof(triangle));
}

//Renders the frame
//This function does not yet take any camera parameter into account
//It just behaves as if the camera was at ORIGIN with angle [0f, 0f]
void renderMesh (mesh* mesh)
{

    if (renderImage == NULL)
    {
        //We have no way to display the image, why bother?
        printf ("ERROR: renderImage is not initialized\n");
        return;
    }

    processRotationMatrix (thetra, zRotationMatrix, xRotationMatrix);

    for (int i = 0; i < mesh->trisc; i++)
    {
        triangle currentTriangle = *(mesh->tris + i);

        //Rotate on Z axis
        triRotatedZ->vertex[0] = multiplyMatrixVector(currentTriangle.vertex[0], *zRotationMatrix);
        triRotatedZ->vertex[1] = multiplyMatrixVector(currentTriangle.vertex[1], *zRotationMatrix);
        triRotatedZ->vertex[2] = multiplyMatrixVector(currentTriangle.vertex[2], *zRotationMatrix);

        *triRotatedZX = *triRotatedZ;

        //Rotate on X axis
        triRotatedZX->vertex[0] = multiplyMatrixVector(triRotatedZ->vertex[0], *xRotationMatrix);
        triRotatedZX->vertex[1] = multiplyMatrixVector(triRotatedZ->vertex[1], *xRotationMatrix);
        triRotatedZX->vertex[2] = multiplyMatrixVector(triRotatedZ->vertex[2], *xRotationMatrix);

        //Offset?
        *triTranslated = *triRotatedZX;

        triTranslated->vertex[0].z = triRotatedZX->vertex[0].z + OBJECTDISTANCE;
        triTranslated->vertex[1].z = triRotatedZX->vertex[1].z + OBJECTDISTANCE;
        triTranslated->vertex[2].z = triRotatedZX->vertex[2].z + OBJECTDISTANCE;

        //Project 3D -> 2D
        *triProjected = *triTranslated;

        triProjected->vertex[0] = multiplyMatrixVector(triTranslated->vertex[0], projectionMatrix);
        triProjected->vertex[1] = multiplyMatrixVector(triTranslated->vertex[1], projectionMatrix);
        triProjected->vertex[2] = multiplyMatrixVector(triTranslated->vertex[2], projectionMatrix);

        //Scale to viewport
        triProjected->vertex[0].x += 1.0f;
        triProjected->vertex[0].y += 1.0f;

        triProjected->vertex[1].x += 1.0f;
        triProjected->vertex[1].y += 1.0f;

        triProjected->vertex[2].x += 1.0f;
        triProjected->vertex[2].y += 1.0f;

        triProjected->vertex[0].x *= 0.5f * (float) WIDTH;
        triProjected->vertex[0].y *= 0.5f * (float) HEIGHT;

        triProjected->vertex[1].x *= 0.5f * (float) WIDTH;
        triProjected->vertex[1].y *= 0.5f * (float) HEIGHT;

        triProjected->vertex[2].x *= 0.5f * (float) WIDTH;
        triProjected->vertex[2].y *= 0.5f * (float) HEIGHT;

        //Draw the result on screen
        drawTriangle(triProjected);
    }
}
