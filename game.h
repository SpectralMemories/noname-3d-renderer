#ifndef GAME
    #define GAME

    #include <SFML/Graphics.h>

    void initializeGame (sfRenderWindow* window, sfImage* image);
    void updateGame (void);
#endif // GAME
