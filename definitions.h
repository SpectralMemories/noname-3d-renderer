#ifndef DEFINITIONS
    #define DEFINITIONS

    #define FRAMEDELTA 16000

    #define PI 3.14159f

    #define WINDOWNAME "3D_Renderer_Test"
    #define WINDOWSTYLE sfFullscreen
    #define WIDTH 1920
    #define HEIGHT 1080
    #define COLORDEPTH 32

    #define BACKGROUND sfBlack
    #define FOREGROUND sfWhite

    #define NEAR 0.5f
    #define FAR 1000.0f
    #define FOV 90.0f

    #define OBJECTDISTANCE 2.0f

    typedef struct {
        float x,y,z;
    } vector3;

    typedef struct {
        vector3 vertex[3];
    } triangle;

    typedef struct {
        vector3 anchor;
        int trisc;
        triangle* tris;
    } mesh;

    typedef struct {
        float m[4][4];
    } matrix4x4;
#endif // DEFINITIONS
