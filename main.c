#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h> //UNIX specific

#include <SFML/Graphics.h>
#include "definitions.h"
#include "game.h"

int main()
{
    sfImage* backImage;
    sfColor backColor = BACKGROUND;

    backImage = sfImage_createFromColor(WIDTH, HEIGHT, backColor);
    const sfImage* originalImage = sfImage_createFromColor(WIDTH, HEIGHT, backColor);

    sfSprite* screenSprite = sfSprite_create();

    sfVideoMode videomode = {WIDTH, HEIGHT, COLORDEPTH};
    sfRenderWindow* window = sfRenderWindow_create(videomode, WINDOWNAME, WINDOWSTYLE, NULL);
    sfEvent windowEvent;

    initializeGame(window, backImage);

    if(!window)
    {
        return EXIT_FAILURE;
    }

    while (sfRenderWindow_isOpen(window))
    {
        while (sfRenderWindow_pollEvent(window, &windowEvent))
        {
            if(windowEvent.type == sfEvtClosed)
            {
                sfRenderWindow_close(window);
            }
        }
        sfRenderWindow_clear(window, BACKGROUND);

        sfImage_copyImage(backImage, originalImage, 0, 0, (sfIntRect) {0, 0, 0, 0}, sfFalse);

        updateGame(); //This rotates the cube AND updates the backImage data

        sfTexture* tex;
        if((tex = sfSprite_getTexture(screenSprite)) != NULL)
        {
            sfTexture_destroy(tex);
        }

        sfSprite_setTexture(screenSprite, sfTexture_createFromImage(backImage, NULL), false);

        sfRenderWindow_drawSprite(window, screenSprite, NULL);
        sfRenderWindow_display(window);
        usleep (FRAMEDELTA); //Limit fps at 60. UNIX specific.
    }

    sfImage_destroy(backImage);
    sfSprite_destroy(screenSprite);
    sfRenderWindow_destroy(window);

    return EXIT_SUCCESS;
}
